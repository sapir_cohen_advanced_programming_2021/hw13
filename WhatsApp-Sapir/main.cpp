#pragma comment (lib, "ws2_32.lib")

#include "Server.h"
#include "WSAInitializer.h"
#include <exception>
#include <iostream>

int main()
{
	while (true) //server always working
	{
		try
		{
			WSAInitializer wsaInit;
			Server s;
			s.serve(8826);
		}
		catch (std::exception& e)
		{
			std::cout << "Error occured: " << e.what() << std::endl;
		}
	}
}