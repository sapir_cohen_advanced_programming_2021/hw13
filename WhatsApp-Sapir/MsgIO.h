#pragma once
#include <iostream>
#include <WinSock2.h>
#include <map>

class MsgIO
{
public:
	MsgIO();
	~MsgIO();

	void addUserName(std::string name, SOCKET soc);
	void removeUserName(std::string name);

	std::string getAllUserNames();

	std::map<std::string, SOCKET> _user_names;

};

