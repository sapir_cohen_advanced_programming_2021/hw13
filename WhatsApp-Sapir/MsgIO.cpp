#include "MsgIO.h"

MsgIO::MsgIO() {	}

MsgIO::~MsgIO()
{
	this->_user_names.clear();
}

void MsgIO::addUserName(std::string name, SOCKET soc)
{
	this->_user_names.insert(std::make_pair(name, soc));
}

void MsgIO::removeUserName(std::string name)
{
	this->_user_names.erase(name);
}

std::string MsgIO::getAllUserNames()
{
	std::string output = "";
	for (std::map<std::string, SOCKET>::iterator it = this->_user_names.begin(); it != this->_user_names.end(); ++it) output += it->first + "&";
	if (!this->_user_names.empty()) output.erase(output.size() - 1, output.size());
	return output;
}
