#include "Helper.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <condition_variable>
#include <mutex>

std::condition_variable cv;
std::mutex mtxQ;
std::mutex mtx;

#define NAME_PLACE 5
#define LEN_PLACE 3
#define LEN_INFO_SIZE 2
#define MSG_INFO_SIZE 5

using std::string;

// recieves the type code of the message from socket (3 bytes)
// and returns the code. if no message found in the socket returns 0 (which means the client disconnected)
int Helper::getMessageTypeCode(SOCKET sc)
{
	char* s = getPartFromSocket(sc, 3);
	std::string msg(s);

	if (msg == "")
		return 0;

	int res = std::atoi(s);
	delete s;
	return  res;
}


void Helper::send_update_message_to_client(SOCKET sc, const string& file_content, const string& second_username, const string &all_users)
{
	//TRACE("all users: %s\n", all_users.c_str())
	const string code = std::to_string(MT_SERVER_UPDATE);
	const string current_file_size = getPaddedNumber(file_content.size(), 5);
	const string username_size = getPaddedNumber(second_username.size(), 2);
	const string all_users_size = getPaddedNumber(all_users.size(), 5);
	const string res = code + current_file_size + file_content + username_size + second_username + all_users_size + all_users;
	//TRACE("message: %s\n", res.c_str());
	sendData(sc, res);
}

// recieve data from socket according byteSize
// returns the data as int
int Helper::getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	return atoi(s);
}

// recieve data from socket according byteSize
// returns the data as string
string Helper::getStringPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	string res(s);
	return res;
}

// return string after padding zeros if necessary
string Helper::getPaddedNumber(int num, int digits)
{
	std::ostringstream ostr;
	ostr << std::setw(digits) << std::setfill('0') << num;
	return ostr.str();
	
}

// recieve data from socket according byteSize
// this is private function
char* Helper::getPartFromSocket(SOCKET sc, int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

char* Helper::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}

// send data to socket
// this is private function
void Helper::sendData(SOCKET sc, std::string message)
{
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

char* Helper::getDataFromClient(SOCKET sc, int bytesNum)
{
	char* buffer = new char[bytesNum];
	recv(sc, buffer, bytesNum, 0);
	return buffer;
}

std::string Helper::getName(char* buffer)
{
	
	std::string name = string(buffer);
	name.erase(0, NAME_PLACE);
	name.erase(Helper::getNameLen(buffer), name.size());
	return name;
}

int Helper::getNameLen(char* buffer)
{
	char len[LEN_INFO_SIZE];
	for (int i = 0; i < LEN_INFO_SIZE; i++) len[i] = buffer[LEN_PLACE + i];
	return atoi(len);
}

int Helper::getMsgLen(char* buffer)
{
	char len[MSG_INFO_SIZE];
	for (int i = 0; i < MSG_INFO_SIZE; i++) len[i] = buffer[NAME_PLACE + Helper::getNameLen(buffer) + i];
	return atoi(len);
}

std::string Helper::getMsg(char* buffer)
{
	std::string msg;
	for (int i = NAME_PLACE + Helper::getNameLen(buffer) + MSG_INFO_SIZE ; i < NAME_PLACE + Helper::getNameLen(buffer) + MSG_INFO_SIZE + Helper::getMsgLen(buffer); i++) msg += buffer[i];
	return msg;
}

std::string Helper::getFileName(std::string first_username, std::string second_username)
{
	if (first_username > second_username) return second_username + "&" + first_username + ".txt";
	else return first_username + "&" + second_username + ".txt";
}

void Helper::addQMsgsToFile(std::map<std::string, std::queue<std::string>>& msgs, std::string first_username, std::string second_username)
{
	std::ofstream file;
	std::unique_lock<std::mutex> file_lock(mtx);
	file.open(Helper::getFileName(first_username, second_username), std::ios_base::app);
	while (msgs[first_username].empty() == false)
	{
		std::unique_lock<std::mutex> mtxUl(mtxQ);
		file << "&MAGSH_MESSAGE&&Author&" << first_username << "&DATA&" << msgs[first_username].front();
		msgs[first_username].pop();
		mtxUl.unlock();
	}
	file.close();
	file_lock.unlock();
}

void Helper::addToQ(std::map<std::string, std::queue<std::string>>& msgs, std::string sender, std::string newMsg)
{
	std::unique_lock<std::mutex> mtxUlq(mtxQ);
	msgs[sender].push(newMsg);
	mtxUlq.unlock();
	cv.notify_one();
}

std::string Helper::getFileContent(std::string first_username, std::string second_username)
{
	std::string data;
	std::unique_lock<std::mutex> file_lock(mtx);
	std::ifstream file(Helper::getFileName(first_username, second_username));
	getline(file, data);
	file_lock.unlock();
	file.close();
	return data;
}
