#include "Server.h"
#include "WSAInitializer.h"
#include <exception>
#include <iostream>
#include <string>
#include <fstream>
#include <thread>
#include <map>
#include "Helper.h"
#include "MsgIO.h"
#include <mutex>

MsgIO ms;
std::map<std::string, std::queue<std::string>> msgs;
std::mutex file_mtx;

#define MAX_BTS 200
#define UPDATE 200

Server::Server()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	std::cout << "starting..." << std::endl;

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	std::cout << "binded" << std::endl;
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "listening..." << std::endl;

	while (true) accept();
}

void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);
	std::cout << "accepting client..." << std::endl;
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	// the function that handle the conversation with the client
	std::thread t(clientHandler, client_socket);
	t.detach();
}


void Server::clientHandler(SOCKET clientSocket)
{
	std::string name, data;
	char* buffer;
	std::ifstream file;

	try
	{
		std::cout << "client accepted!" << std::endl;

		//getting Login message (the name)
		name = Helper::getName(Helper::getDataFromClient(clientSocket, MAX_BTS));

		//adding client to client set
		ms.addUserName(name, clientSocket);
		std::cout << "ADDED new client " << clientSocket << ", " << name << " to clients list" << std::endl;

		//sending ServerUpdateMessage
		Helper::send_update_message_to_client(clientSocket, "", "", ms.getAllUserNames());
		
		while (true)
		{
			//getting (buffer) information requests from client
			buffer = Helper::getDataFromClient(clientSocket, MAX_BTS);

			//sending messages
			if (Helper::getMsgLen(buffer) != 0)
			{
				//update chat
				Helper::send_update_message_to_client(ms._user_names[Helper::getName(buffer)], data, name, ms.getAllUserNames());
				Helper::send_update_message_to_client(clientSocket, data, Helper::getName(buffer), ms.getAllUserNames());

				//getting message and adding to queue
				Helper::addToQ(std::ref(msgs), name, Helper::getMsg(buffer));

				//thread that adding messages to their file
				Helper::addQMsgsToFile(std::ref(msgs), name, Helper::getName(buffer));

				//sending update message

				data = Helper::getFileContent(name, Helper::getName(buffer));

				//update after sending
				Helper::send_update_message_to_client(ms._user_names[Helper::getName(buffer)], data, name, ms.getAllUserNames());
				Helper::send_update_message_to_client(clientSocket, data, Helper::getName(buffer), ms.getAllUserNames());
			}
			
			//update for user
			Helper::send_update_message_to_client(clientSocket, "", "", ms.getAllUserNames());
			Sleep(UPDATE);

		}
		
	}
	catch (const std::exception& e) //client logged out
	{
		std::cout << "recieved exit message from client" << std::endl;
		closesocket(clientSocket);
		ms.removeUserName(name);
		std::cout << "REMOVED " << clientSocket << ", " << name << " from clients list" << std::endl;
	}
}
